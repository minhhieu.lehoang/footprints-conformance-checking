#Preprocess the eventlog
from pm4py.algo.filtering.log.timestamp import timestamp_filter as tf_log
from pm4py.algo.filtering.pandas.timestamp import timestamp_filter as tf_df
from pm4py.algo.filtering.log.start_activities import start_activities_filter as startf_log
from pm4py.algo.filtering.log.end_activities import end_activities_filter as endf_log
from pm4py.algo.filtering.pandas.start_activities import start_activities_filter as startf_df
from pm4py.algo.filtering.pandas.end_activities import end_activities_filter as endf_df


def contain_in_interval_log(log, start, finish):
    """

    :param log: event log
    :param start: start date in string with format "Y-M-D h:m:s"
    :param finish: finish date in string with format "Y-M-D h:m:s"
    :return: traces in interval [start, finish]
    """
    filtered_log = tf_log.filter_traces_contained(log, start, finish)
    return  filtered_log

def contain_in_interval_df(dataframe, start, finish):
    """

    :param dataframe: data frame
    :param start: start date in string with format "Y-M-D h:m:s"
    :param finish: finish date in string with format "Y-M-D h:m:s"
    :return: traces in interal [start, finish]
    """
    df_timest_intersecting = tf_df.filter_traces_intersecting(dataframe, start, finish
                                                                         , parameters = {timestamp_filter.Parameters.CASE_ID_KEY: "case:concept:name",
                                                                                         timestamp_filter.Parameters.TIMESTAMP_KEY: "time:timestamp"})
    return  df_timest_intersecting

def start_activities_log(log):
    """

    :param log: event logs
    :return: dict with formal: {activity : number as starting activity}
    """
    log_start = startf_log.get_start_activities(log)
    return log_start

def start_activities_df(dataFrame):
    """

    :param dataFrame: data frame
    :return: dict with formal: {activity : number as starting activity}
    """
    df_start = startf_df.get_start_activities(dataFrame)
    return df_start

def end_activities_log(log):
    """

    :param log: event logs
    :return: dict with formal: {activity : number as ending activity}
    """
    end_acitivites = endf_log.get_end_activities(log)
    return end_acitivites

def end_activities_df(dataFrame):
    """

    :param dataFrame: data frame
    :return: dict with formal: {activity : number as ending activity}
    """
    end_acitivites = endf_df.get_end_activities(dataFrame)
    return end_acitivites

def start_with_log(log, listOfStart):
    """

    :param log: event log
    :param listOfStart: list of start activities
    :return: log with traces starting with the activities in listOfStart
    """
    filtered_log = []
    for act in listOfStart:
        filtered_log += startf_log.apply(log, act)
    return filtered_log

def start_with_df(dataFrame, listOfStart):
    """

    :param dataFrame: data frame
    :param listOfStart: list of start activities
    :return: log with traces starting with the activities in listOfStart
    """
    filtered_log = []
    for act in listOfStart:
        filtered_log += startf_df.apply(dataFrame, act)
    return filtered_log

def end_with_log(log, listOfEnd):
    """

    :param log: event log
    :param listOfEnd: list of end activities
    :return: log with traces ending with the activities in listOfStart
    """
    filtered_log = []
    for act in listOfEnd:
        filtered_log += endf_log.apply(log, act)
    return filtered_log

def end_with_df(dataFrame, listOfEnd):
    """

    :param dataFrame: data frame
    :param listOfEnd: list of end activities
    :return: log with traces ending with the activities in listOfStart
    """
    filtered_log = []
    for act in listOfEnd:
        filtered_log += endf_df.apply(dataFrame, act)
    return filtered_log

