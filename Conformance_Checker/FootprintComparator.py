import pandas as pd
import numpy as np


def compare_index(footprint1, footprint2):
    """
    This function checks if the index of two footprints are the same
    :param footprint1: the first footprint matrix
    :param footprint2: the second footprint matrix
    :return: -1 if not euqal 0 if equal
    """
    index1 = footprint1.index.values
    index2 = footprint2.index.values
    return np.array_equal(index1, index2)


def compare_columns(footprint1, footprint2):
    """
    This function checks if the columns of two footprints are the same
    :param footprint1: the first footprint matrix
    :param footprint2: the second footprint matrix
    :return: Flase if not equal True if equal
    """
    columns1 = footprint1.columns.values 
    columns2 = footprint2.columns.values
    return np.array_equal(columns1, columns2)


def compare_footprints(footprint1, footprint2, name):
    """
    This function compares two footprints and returns a matrix with values 0 and 1
    0: no deviation 1: deviation
    :param name: matrix name
    :param footprint1: the first footprint matrix to compare as a pandas dataframe
    :param footprint2: the second footprint matrix to compare as a pandas dataframe
    :return: result: a pandas dataframe containing the information of deviations
            if index is not the same we return error code -1
            if columns are not the same we return error code -2
    """
    if not compare_index(footprint1, footprint2):
        return -1
    if not compare_columns(footprint1, footprint2):
        return -2
    index1 = footprint1.index.values
    column1 = footprint1.columns.values
    values1 = footprint1.values
    values2 = footprint2.values
    result_values = np.arange(len(index1)**2).reshape(len(index1), len(index1))
    for i in range(len(index1)):
        for j in range(len(index1)):
            if values1[i, j] != values2[i, j]:
                result_values[i, j] = 1
            else:
                result_values[i, j] = 0

    result = pd.DataFrame(data=result_values, index=index1, columns=column1, dtype=int)

    return result

def highlight_diff(df1, df2, color = 'pink'):
    attr = 'background-color: {}'.format(color)
    return pd.DataFrame(np.where(df1.ne(df2), attr, ''),
                        index=df1.index, columns=df1.columns)

if __name__ == '__main__':
    raw_result1 = np.array([[4, 1, 2, 2, 2, 2],
                           [2, 4, 1, 1, 1, 1],
                           [1, 2, 4, 3, 1, 3],
                           [1, 2, 3, 4, 3, 4],
                           [1, 2, 2, 3, 4, 3],
                           [1, 2, 3, 4, 3, 4]])

    result1 = pd.DataFrame(data=raw_result1[0:, 0:],
                          index=["a", "b", "c", "d", "e", "f"],
                          columns=["a", "b", "c", "d", "e", "f"])

    raw_result2 = np.array([[4, 1, 2, 2, 2, 2],
                           [2, 4, 1, 1, 1, 1],
                           [1, 2, 4, 2, 1, 3],
                           [1, 2, 3, 1, 3, 4],
                           [1, 3, 2, 3, 4, 3],
                           [1, 2, 3, 4, 3, 4]])

    result2 = pd.DataFrame(data=raw_result2[0:, 0:],
                          index=["a", "b", "c", "d", "e", "f"],
                          columns=["a", "b", "c", "d", "e", "f"])

    print(compare_footprints(result1, result2, 'running_ex_compare'))