from pm4py.algo.discovery.inductive import algorithm as inductive_miner
from pm4py.visualization.process_tree import visualizer as pt_visualizer

import graphviz, os, pathlib

os.environ["PATH"] += os.pathsep + 'D:/graphviz-2.38/release/bin'

path = os.path.join(pathlib.Path(os.getcwd()).parent, 'server', 'static', 'images')


def print_process_tree(event_log, name):
    tree = inductive_miner.apply_tree(event_log)

    gviz = pt_visualizer.apply(tree)
    gviz.render(filename=name, directory=path, format='png')
    return


def extract_process_tree(event_log):
    return inductive_miner.apply_tree(event_log)
