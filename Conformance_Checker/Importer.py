import os

from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.objects.log.importer.csv import factory as csv_importer
from pm4py.objects.conversion.log import factory as log_conversion


"""
Importer functions for loading eventlogs and Process trees
"""


def import_xes_log(file_name='running-example.xes'):
    """
    An Importer Function for eventlogs given in the .xes extension
    :param file_name: the filename of the eventlog to be imported.
            needs to be present in the "tests/test_input/synthetic" directory
    :return: Event_log object given by Pm4Py
    """
    file_path = os.path.join("..", "tests", "test_input", "synthetic", file_name)
    xes_log = xes_importer.apply(file_path)
    return xes_log


def import_csv_log(file_name='running-example.csv'):
    """
    An Importer Function for eventlogs given in the .csv extension
    :param file_name: the filename of the eventlog to be imported.
            needs to be present in the "tests/test_input/synthetic" directory
    :return: Event_log object given by Pm4Py
    """
    file_path = os.path.join("..", "tests", "test_input", "synthetic", file_name)
    csv_log = csv_importer.apply(file_path)
    event_log = log_conversion.apply(csv_log, variant=log_conversion.TO_EVENT_LOG)

    return event_log


def import_process_tree(file_name='pt_running_example.txt'):
    """
    An Import Function for process tree saved as string representations
    :param file_name: The filename ofthe file containing the process tree
    :return: returns a process tree datastructure provided by pm4py
    """
    file_path = os.path.join("..", "tests", "test_input", "process-trees", file_name)
    file = open(file_path, "r")
    process_tree_string = file.readline()
    return process_tree_string



