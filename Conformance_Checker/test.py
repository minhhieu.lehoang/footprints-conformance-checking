import pandas as pd
import numpy as np
from pm4py.objects.process_tree.util import parse
from pm4py.objects.process_tree import process_tree as pt



example_tree_string = +( 'a', ->( 'b', +( ->( *( 'c', τ ), X( 'e', τ ) ), X( 'd', 'f' ) ) ) )
example_tree = parse(example_tree_string)
print(example_tree._get_leaves())
expected_leaves = [pt.ProcessTree(label='a'), ]
print(expected_leaves)

