import pm4py.objects.process_tree.util as ut
from pm4py.algo.filtering.log.attributes import attributes_filter


def get_activities_from_log(eventlog):
    """
    This function extracts a list from an eventlog with all unique activities
    :param eventlog: an eventlog object from pm4py
    :return: a list of activities ordered descending alphabetically
    """
    activities_dict = attributes_filter.get_attribute_values(eventlog, 'concept:name')
    activities_list = []
    for key in activities_dict.keys():
        activities_list.append(key)
    activities_list.sort()
    return activities_list


def remove_tau_leaves(leaves):
    """
    This function removes any tau leave entries in a leave list for a process tree
    :param leaves: a list of leave nodes extracted from a process tree
    :return: a list of leaves without any tau-leaves left
    """
    for leave in leaves:
        if leave.label is None:
            leaves.remove(leave)

    return leaves


def extract_activity_names(leaves):
    """
    This function extracts the activity names of a leave list of process trees and sort it alphabetically
    :param leaves: A list of leave nodes of a process tree without tau-leaves
    :return: a list of unique activity names
    """
    activity_list = []
    leaves = remove_tau_leaves(leaves)
    for leave in leaves:
        activity_list.append(leave.label)
    activity_list.sort()
    return activity_list


def get_leaves(tree):
    """
    This function extracts all the nodes that are leaves for a given process tree
    :param tree: a list of children of a root node of a process tree
    :return: a list of leave nodes of the process tree
    """
    leaves = []
    for child in tree.children:
        if ut.is_leaf(child):
            leaves.append(child)
        else:
            child_leaves = get_leaves(child)
            for leave in child_leaves:
                leaves.append(leave)
    return leaves
