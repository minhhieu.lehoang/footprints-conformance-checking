from flask import Flask, render_template, request, flash, redirect, url_for, send_from_directory
import pandas as pd
import os, pathlib

from pm4py.objects.conversion.log import converter as log_conversion

from Conformance_Checker import FootprintGenerator as fg
from Conformance_Checker import ProcessTree as pt
from Conformance_Checker import FootprintComparator as fc


from werkzeug.utils import secure_filename
import time

path = pathlib.Path(os.getcwd()).parent
UPLOAD_FOLDER = os.path.join(path,"Conformance_Checker", "database", "uploaded files")
ALLOWED_EXTENSIONS = {'csv', 'xes'}

server = Flask(__name__)
server.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# home page of our server
# @server.route('/', methods=['GET', 'POST'])
# def index():
#     return render_template('index.html')

@server.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            list_file = [f for f in os.listdir(UPLOAD_FOLDER) if os.path.isfile(os.path.join(UPLOAD_FOLDER, f))]
            if len(list_file) > 0:
                for f in list_file:
                    os.remove(os.path.join(UPLOAD_FOLDER, f))
            file.save(os.path.join(server.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return render_template('index.html')

@server.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(server.config['UPLOAD_FOLDER'],
                               filename)
# process tree page
@server.route('/process_tree')
def process_tree():
    #time before
    t_pt_1 = time.time()
    file = [f for f in os.listdir(UPLOAD_FOLDER) if os.path.isfile(os.path.join(UPLOAD_FOLDER, f))][0]
    df = pd.read_csv(os.path.join(UPLOAD_FOLDER, file), sep=',')
    event_log = log_conversion.apply(df)

    image_name = file.split('.')[0]
    image_file = image_name + '.png'
    pt.print_process_tree(event_log, image_name)
    #time after
    t_pt_2 = time.time()
    t = t_pt_2 - t_pt_1
    print("Time to extract Process Tree is: " + str(t))

    return render_template('process_tree.html', user_image=os.path.join('static', 'images', image_file))


@server.route('/footprint_matrices')
def footprint_matrices():
    #time before
    t_fm_1 = time.time()
    file = [f for f in os.listdir(UPLOAD_FOLDER) if os.path.isfile(os.path.join(UPLOAD_FOLDER, f))][0]
    df = pd.read_csv(os.path.join(UPLOAD_FOLDER, file), sep=',')

    event_log = log_conversion.apply(df)
    fm1 = fg.extract_footprint_from_eventlog(event_log)
    #time after
    t_fm_2 = time.time()
    t1 = t_fm_2 - t_fm_1
    print("Time to extract Footprint mactrice from eventlog is: " + str(t1))
    tree = pt.extract_process_tree(event_log)
    #time before
    t_fm_3 = time.time()
    fm2 = fg.extract_footprint_from_process_tree(tree)
    #time after
    t_fm_4 = time.time()
    t2 = t_fm_4 - t_fm_3
    print("Time to extract Footprint mactrice from process tree is: " + str(t2))
    fm3 = fm2.style.apply(fc.highlight_diff, axis=None, df2=fm1)

    return render_template('footprint_matrices.html', tables=[fm1.to_html(classes='input'), fm3.render()],
                           titles=['na', 'Footprint matrix from the input', 'Footprint matrix from the process tree '
                                                                            'with the highlight of deviation'])


if __name__ == '__main__':
    server.run(debug=True)
