import unittest
import os
import flask
import server as sr
import templates
from flask import Flask, render_template

class serverTester(unittest.TestCase):

#test if footprintpage load correctly
def test_footprintpage(self):
        self.asserEqual(True, sr.footprint_matrices() == render_template('footprint_matrics.html'),"footprint tree fender failure")



 #test if process tree load correctly
    def test_processtreepage(self):
        self.assertEqual(True,sr.process_tree() == render_template('process_tree.html'),"process tree render failure") 
if __name__ == '__main__':
    unittest.main()
   
