from unittest import TestCase
import Conformance_Checker.GeneratorUtil as gu
from pm4py.objects.process_tree import process_tree as pt
from pm4py.objects.process_tree.util import parse
import Conformance_Checker.Importer as imp


class Test(TestCase):
    example_tree_string = "+( 'a', ->( 'b', +( ->( *( 'c', τ ), X( 'e', τ ) ), X( 'd', 'f' ) ) ) )"
    example_tree = parse(example_tree_string)

    def test_remove_tau_leaves(self):
        leaves = gu.get_leaves(self.example_tree)
        result_leaves = [pt.ProcessTree(label='a'), pt.ProcessTree(label='b'), pt.ProcessTree(label='d'),
                         pt.ProcessTree(label='f'), pt.ProcessTree(label='c'), pt.ProcessTree(label='e')]
        new_leaves = gu.remove_tau_leaves(leaves)
        for i in range(6):
            self.assertIn(new_leaves[i], result_leaves)

    def test_extract_activity_names(self):
        leaves = gu.get_leaves(self.example_tree)
        leaves = gu.remove_tau_leaves(leaves)
        activities = gu.extract_activity_names(leaves)
        expected_activities = ['a', 'b', 'c', 'd', 'e', 'f']
        for i in range(6):
            self.assertIn(activities[i], expected_activities, msg='%d:  activity name does not match' % i)

    def test_get_leaves(self):
        extracted_leaves = gu.get_leaves(self.example_tree)
        expected_leave_labels = ['a', 'b', 'c', 'd', 'e', 'f', None]
        for i in range(8):
            self.assertIn(extracted_leaves[i].label, expected_leave_labels,
                          msg="%d: A Leave label does not match" % i)

    # this test tests if get_activities returns a list of all activities ordered
    # descending alphabetically for running example
    def test_get_activities_from_log(self):
        activities = gu.get_activities_from_log(imp.import_xes_log())
        result_activities = ["check ticket", "decide", "examine casually", "examine thoroughly", "pay compensation",
                             "register request", "reinitiate request", "reject request"]

        self.assertEqual(activities, result_activities, msg="running-example: activities are not correct")
