import unittest
import numpy as np
import pandas as pd
import Conformance_Checker.FootprintComparator as fc


class MyTestCase(unittest.TestCase):
    footprintTestValues1 = np.array([[4, 1, 2, 2, 2, 2],
                           [2, 4, 1, 1, 1, 1],
                           [1, 2, 4, 3, 1, 3],
                           [1, 2, 3, 4, 3, 4],
                           [1, 2, 2, 3, 4, 3],
                           [1, 2, 3, 4, 3, 4]])

    footprintTestValues2 = np.array([[4, 2, 3, 2, 2, 2],
                                     [2, 4, 1, 1, 4, 1],
                                     [1, 1, 4, 3, 1, 2],
                                     [1, 2, 3, 4, 1, 4],
                                     [3, 2, 2, 3, 4, 3],
                                     [1, 2, 3, 4, 3, 4]])

    footprintResultValues = np.array([[0, 1, 1, 0, 0, 0],
                                     [0, 0, 0, 0, 1, 0],
                                     [0, 1, 0, 0, 0, 1],
                                     [0, 0, 0, 0, 1, 0],
                                     [1, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0]])

    testResult = pd.DataFrame(data=footprintResultValues[0:, 0:],
                                  index=["a", "b", "c", "d", "e", "f"],
                                  columns=["a", "b", "c", "d", "e", "f"])

    # tests if we can succesfully detect equal index
    def test_checkEqualIndex(self):
        testFootprint1 = pd.DataFrame(data=self.footprintTestValues1[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])
        testFootprint2 = pd.DataFrame(data=self.footprintTestValues2[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])

        self.assertEqual(True, fc.compare_index(testFootprint1, testFootprint2), msg="Index should be the same")

    # tests if we can succesfully detect unequal index
    def test_checkUnequalIndex(self):
        testFootprint1 = pd.DataFrame(data=self.footprintTestValues1[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])
        testFootprint2 = pd.DataFrame(data=self.footprintTestValues2[0:, 0:],
                                      index=["a", "b", "c", "h", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])
        self.assertEqual(False, fc.compare_index(testFootprint1, testFootprint2), msg="Index should not be the same")

    # tests if we can succesfully detect equal columns
    def test_checkEqualColumns(self):
        testFootprint1 = pd.DataFrame(data=self.footprintTestValues1[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])
        testFootprint2 = pd.DataFrame(data=self.footprintTestValues2[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])
        self.assertEqual(True, fc.compare_columns(testFootprint1, testFootprint2), msg="Columns should be the same")

    # tests if we can succesfully detect unequal columns
    def test_checkUnequalColumns(self):
        testFootprint1 = pd.DataFrame(data=self.footprintTestValues1[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])
        testFootprint2 = pd.DataFrame(data=self.footprintTestValues2[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "h", "e", "f"])
        self.assertEqual(False, fc.compare_columns(testFootprint1, testFootprint2), msg="Columns should not be the same")

    # tests if we get the correct comparison results
    def test_result_values(self):
        testFootprint1 = pd.DataFrame(data=self.footprintTestValues1[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])
        testFootprint2 = pd.DataFrame(data=self.footprintTestValues2[0:, 0:],
                                      index=["a", "b", "c", "d", "e", "f"],
                                      columns=["a", "b", "c", "d", "e", "f"])

        compared = fc.compare_footprints(testFootprint1, testFootprint2)
        size = len(testFootprint1.index)
        for i in range(size):
            for j in range(size):
                self.assertEqual(self.testResult.values[i, j], compared.values[i, j], msg="values should match")


if __name__ == '__main__':
    unittest.main()
