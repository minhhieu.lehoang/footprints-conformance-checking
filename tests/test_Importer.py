from unittest import TestCase
from Conformance_Checker.Importer import import_xes_log, import_csv_log, import_process_tree, export_process_tree
from pm4py.objects.process_tree.util import parse


class Test(TestCase):
    # test if the standard import for .xes is not None
    def test_xes_standard_import_is_not_None(self):
        event_log = import_xes_log()
        self.assertIsNotNone(event_log, "Standard imported Eventlog is None!")

    # test if the standard import for .csv is not None
    def test_csv_standard_import_is_not_None(self):
        event_log = import_csv_log()
        self.assertIsNotNone(event_log, "Standard imported Eventlog is None!")

    # tests if we can import a process tree representation from a file
    def test_import_process_tree(self):
        process_tree_rep = import_process_tree()
        tree = parse(process_tree_rep)
        print(tree)
        equal = tree.__eq__(parse("+( 'a', ->( 'b', +( ->( *( 'c', 'tau' ), X( 'e', 'tau' ) ), X( 'd', 'f' ) ) ) )"))
        self.assertEqual(True, equal,
                         msg="String rep should be the same")

