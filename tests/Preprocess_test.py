from pm4py.objects.log.importer.xes import importer as xes_importer
import pandas as pd
from pm4py.objects.log.util import dataframe_utils
import unittest
import Conformance_Checker.Preprocess as pre
import os
file_path_csv = os.path.join("..", "tests", "test_input", "synthetic", "running-example.csv")
file_path_xes = os.path.join("..", "tests", "test_input", "synthetic", "running-example.xes")

log_csv = pd.read_csv(file_path_csv, sep=',')
log_csv = dataframe_utils.convert_timestamp_columns_in_df(log_csv)
log_csv = log_csv.sort_values('time:timestamp')
log = xes_importer.apply(file_path_xes)

class testGenerator(unittest.TestCase):
    def testStartAct(self):
        res = {'register request': 6}
        self.assertEqual(res, pre.start_activities_df(log_csv))
        self.assertEqual(res, pre.start_activities_log(log))
    def testEndAct(self):
        res = {'pay compensation': 3, 'reject request': 3}
        self.assertEqual(res, pre.end_activities_df(log_csv))
        self.assertEqual(res, pre.end_activities_log(log))
if __name__ == '__main__':
    unittest.main()