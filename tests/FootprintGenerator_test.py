import unittest
import Conformance_Checker.FootprintGenerator as fp
import Conformance_Checker.Importer as imp
import numpy as np
import pandas as pd
from pm4py.objects.process_tree.util import parse


class FootprintGeneratorTestCase(unittest.TestCase):
    example_tree_string = "+( 'a', ->( 'b', +( ->( *( 'c', τ ), X( 'e', τ ) ), X( 'd', 'f' ) ) ) )"
    example_tree = parse(example_tree_string)

    # this test tests if the footprint values of running example is correct
    def test_footprint_running_example_value(self):
        raw_result = np.array([['#', '->', '||', '||', '#', '<-', '<-', '#'],
                               ['<-', '#', '<-', '<-', '->', '#', '->', '->'],
                               ['||', '->', '#', '#', '#', '<-', '<-', '#'],
                               ['||', '->', '#', '#', '#', '<-', '<-', '#'],
                               ['#', '<-', '#', '#', '#', '#', '#', '#'],
                               ['->', '#', '->', '->', '#', '#', '#', '#'],
                               ['->', '<-', '->', '->', '#', '#', '#', '#'],
                               ['#', '<-', '#', '#', '#', '#', '#', '#']])
        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["check ticket", "decide", "examine casually", "examine thoroughly",
                                     "pay compensation", "register request", "reinitiate request", "reject request"],
                              columns=["check ticket", "decide", "examine casually", "examine thoroughly",
                                       "pay compensation", "register request", "reinitiate request", "reject request"])
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log())
        size = len(result.index)
        for i in range(size):
            for j in range(size):
                self.assertEqual(result.values[i, j], footprint.values[i, j], msg="values at "
                                                                                  "(%d, %d) should match" % (i, j))

    # this test tests if the footprint index names of running example is correct
    def test_footprint_running_example_index(self):
        raw_result = np.array([['#', '->', '||', '||', '#', '<-', '<-', '#'],
                               ['<-', '#', '<-', '<-', '->', '#', '->', '->'],
                               ['||', '->', '#', '#', '#', '<-', '<-', '#'],
                               ['||', '->', '#', '#', '#', '<-', '<-', '#'],
                               ['#', '<-', '#', '#', '#', '#', '#', '#'],
                               ['->', '#', '->', '->', '#', '#', '#', '#'],
                               ['->', '<-', '->', '->', '#', '#', '#', '#'],
                               ['#', '<-', '#', '#', '#', '#', '#', '#']])
        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["check ticket", "decide", "examine casually", "examine thoroughly",
                                     "pay compensation", "register request", "reinitiate request",
                                     "reject request"],
                              columns=["check ticket", "decide", "examine casually", "examine thoroughly",
                                       "pay compensation", "register request", "reinitiate request",
                                       "reject request"])
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log())
        self.assertEqual(result.index.all(), footprint.index.all(),
                         msg="running-example: footprints index do not match")

    # this test tests if the footprint column names of running example is correct
    def test_footprint_running_example_Column(self):
        raw_result = np.array([['#', '->', '||', '||', '#', '<-', '<-', '#'],
                               ['<-', '#', '<-', '<-', '->', '#', '->', '->'],
                               ['||', '->', '#', '#', '#', '<-', '<-', '#'],
                               ['||', '->', '#', '#', '#', '<-', '<-', '#'],
                               ['#', '<-', '#', '#', '#', '#', '#', '#'],
                               ['->', '#', '->', '->', '#', '#', '#', '#'],
                               ['->', '<-', '->', '->', '#', '#', '#', '#'],
                               ['#', '<-', '#', '#', '#', '#', '#', '#']])
        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["check ticket", "decide", "examine casually", "examine thoroughly",
                                     "pay compensation", "register request", "reinitiate request",
                                     "reject request"],
                              columns=["check ticket", "decide", "examine casually", "examine thoroughly",
                                       "pay compensation", "register request", "reinitiate request",
                                       "reject request"])
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log())
        self.assertEqual(result.columns.all(), footprint.columns.all(),
                         msg="running-example: footprints column do not match")

    # this test tests if the footprint values matrix of running example has the correct shape
    def test_footprint_running_example_value_shape(self):
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log())
        self.assertEqual(footprint.values.shape, (8, 8))

    # this test tests if the footprint values of ETM_Configuration1 is correct
    def test_footprint_ETM_Configuration1_value(self):
        raw_result = np.array([['#', '->', '->', '->', '#', '#', '#'],
                               ['<-', '#', '||', '||', '->', '->', '#'],
                               ['<-', '||', '#', '||', '->', '->', '#'],
                               ['<-', '||', '||', '#', '->', '->', '#'],
                               ['#', '<-', '<-', '<-', '#', '#', '->'],
                               ['#', '<-', '<-', '<-', '#', '#', '->'],
                               ['#', '#', '#', '#', '<-', '<-', '#']])
        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["A", "B", "C", "D", "E", "F", "G"],
                              columns=["A", "B", "C", "D", "E", "F", "G"])
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log("ETM_Configuration1.xes.gz"))
        size = len(result.index)
        for i in range(size):
            for j in range(size):
                self.assertEqual(result.values[i, j], footprint.values[i, j], msg="values at (%d, %d) "
                                                                                  "should match" % (i, j))

    # this test tests if the footprint index names of ETM_Configuration1 is correct
    def test_footprint_ETM_Configuration1_index(self):
        raw_result = np.array([['#', '->', '->', '->', '#', '#', '#'],
                               ['<-', '#', '||', '||', '->', '->', '#'],
                               ['<-', '||', '#', '||', '->', '->', '#'],
                               ['<-', '||', '||', '#', '->', '->', '#'],
                               ['#', '<-', '<-', '<-', '#', '#', '->'],
                               ['#', '<-', '<-', '<-', '#', '#', '->'],
                               ['#', '#', '#', '#', '<-', '<-', '#']])
        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["A", "B", "C", "D", "E", "F", "G"],
                              columns=["A", "B", "C", "D", "E", "F", "G"])
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log("ETM_Configuration1.xes.gz"))
        self.assertEqual(result.index.all(), footprint.index.all(), msg="ETM_Configuration1: footprints "
                                                                        "index do not match")

    # this test tests if the footprint column names of ETM_Configuration1 is correct
    def test_footprint_ETM_Configuration1_columns(self):
        raw_result = np.array([['#', '->', '->', '->', '#', '#', '#'],
                               ['<-', '#', '||', '||', '->', '->', '#'],
                               ['<-', '||', '#', '||', '->', '->', '#'],
                               ['<-', '||', '||', '#', '->', '->', '#'],
                               ['#', '<-', '<-', '<-', '#', '#', '->'],
                               ['#', '<-', '<-', '<-', '#', '#', '->'],
                               ['#', '#', '#', '#', '<-', '<-', '#']])
        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["A", "B", "C", "D", "E", "F", "G"],
                              columns=["A", "B", "C", "D", "E", "F", "G"])
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log("ETM_Configuration1.xes.gz"))
        self.assertEqual(result.columns.all(), footprint.columns.all(), msg="ETM_Configuration1: footprints "
                                                                            "columns do not match")

    # this test tests if the footprint values matrix of ETM_Configuration1 has the correct shape
    def test_footprint_ETM_Configuration1_value_shape(self):
        footprint = fp.extract_footprint_from_eventlog(imp.import_xes_log("ETM_Configuration1.xes.gz"))
        self.assertEqual(footprint.values.shape, (7, 7))

    # ----------------------------------------------------------------------------------------------------------------
    # From here we test process tree footprint extraction

    # tests if we can extract the correct footprint matrix values from a process tree
    def test_extract_footprint_from_process_tree_value(self):
        raw_result = np.array([['#', '->', '<-', '<-', '<-', '<-'],
                               ['<-', '#', '->', '->', '#', '->'],
                               ['->', '<-', '->', '||', '->', '||'],
                               ['->', '<-', '||', '#', '<-', '#'],
                               ['->', '#', '<-', '->', '#', '->'],
                               ['->', '<-', '||', '#', '<-', '#']])

        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["a", "b", "c", "d", "e", "f"],
                              columns=["a", "b", "c", "d", "e", "f"])

        footprint = fp.extract_footprint_from_process_tree(self.example_tree)
        size = len(result.index)
        for i in range(size):
            for j in range(size):
                self.assertEqual(result.values[i, j], footprint.values[i, j], msg="values at (%d, %d) "
                                                                                  "should match" % (i, j))

    # tests if we build the correct index for the footprint table
    def test_extract_footprint_from_process_tree_index(self):
        raw_result = np.array([['#', '->', '<-', '<-', '<-', '<-'],
                               ['<-', '#', '->', '->', '#', '->'],
                               ['->', '<-', '->', '||', '->', '||'],
                               ['->', '<-', '||', '#', '<-', '#'],
                               ['->', '#', '<-', '->', '#', '->'],
                               ['->', '<-', '||', '#', '<-', '#']])

        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["a", "b", "c", "d", "e", "f"],
                              columns=["a", "b", "c", "d", "e", "f"])

        footprint = fp.extract_footprint_from_process_tree(self.example_tree)
        self.assertEqual(result.index.all(), footprint.index.all(), msg="Footprint index do not match")

    # tests if we build the correct columns for the footprint table
    def test_extract_footprint_from_process_tree_columns(self):
        raw_result = np.array([['#', '->', '<-', '<-', '<-', '<-'],
                               ['<-', '#', '->', '->', '#', '->'],
                               ['->', '<-', '->', '||', '->', '||'],
                               ['->', '<-', '||', '#', '<-', '#'],
                               ['->', '#', '<-', '->', '#', '->'],
                               ['->', '<-', '||', '#', '<-', '#']])

        result = pd.DataFrame(data=raw_result[0:, 0:],
                              index=["a", "b", "c", "d", "e", "f"],
                              columns=["a", "b", "c", "d", "e", "f"])

        footprint = fp.extract_footprint_from_process_tree(self.example_tree)
        self.assertEqual(result.index.all(), footprint.columns.all(), msg="Footprint column do not match")

    def test_is_first_child_skippable_at_start(self):
        pt_rep = "*( τ, 'a')"
        tree = parse(pt_rep)
        skippable = fp.is_first_child_skippable_at_start(tree)
        self.assertTrue(skippable, msg='tree should be skipple at start')

    def test_is_first_child_skippable_at_end(self):
        pt_rep = "*( τ, 'a')"
        tree = parse(pt_rep)
        skippable = fp.is_first_child_skippable_at_end(tree)
        self.assertTrue(skippable, msg='tree should be skippable at end')




    def test_xor_actvities(self):
        xor_tree = "X( 'a', 'd', +( 'c', 'b' ) )"
        tree = parse(xor_tree)
        footprint = fp.extract_footprint_from_process_tree(tree)
        activities = []
        activities.append("a")
        activities.append("b")
        activities.append("c")
        activities.append("d")
        for i in range(4):
            self.assertEqual(activities[i], footprint.index[i], msg="activities should match")

    def test_xor_sequential_footprints(self):
        xor_tree = "X( 'a', 'd', +( 'c', 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        self.assertTrue(len(footprints[0]) == 0, msg="Sequential footprints should be empty")

    def test_xor_parallel_footprints(self):
        xor_tree = "X( 'a', 'd', +( 'c', 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_parallel = [(['c', "ns"], ['b', "ns"]), (['b', "ns"], ['c', "ns"])]
        self.assertEquals(footprints[1], expected_parallel,  msg='parallel footprints should match')

    def test_xor_start_activities(self):
        xor_tree = "X( 'a', 'd', +( 'c', 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_start = [['a', 'ns'], ['d', 'ns'], ['c', 'ns'], ['b', 'ns']]
        self.assertListEqual(sa, expected_start, msg='start activities should match')

    def test_xor_end_activities(self):
        xor_tree = "X( 'a', 'd', +( 'c', 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_end = [['a', 'ns'], ['d', 'ns'], ['c', 'ns'], ['b', 'ns']]
        self.assertListEqual(ea, expected_end, msg='end activities should match')



    def test_and_actvities(self):
        xor_tree = "+( X( τ, 'c' ), ->( X( τ, 'a' ), 'b' ) )"
        tree = parse(xor_tree)
        footprint = fp.extract_footprint_from_process_tree(tree)
        activities = []
        activities.append("a")
        activities.append("b")
        activities.append("c")
        for i in range(3):
            self.assertEqual(activities[i], footprint.index[i], msg="activities should match")

    def test_and_sequential_footprints(self):
        xor_tree = "+( X( τ, 'c' ), ->( X( τ, 'a' ), 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_sequential = [(['a', 's'], ['b', 'ns']), (['c', 's'], ['a', 's'])]
        self.assertListEqual(footprints[0], expected_sequential, msg="Sequential footprints should be empty")

    def test_and_parallel_footprints(self):
        xor_tree = "+( X( τ, 'c' ), ->( X( τ, 'a' ), 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_parallel = [(['c', "s"], ['b', "ns"]), (['b', "ns"], ['c', "s"])]
        self.assertListEqual(footprints[1], expected_parallel,  msg='parallel footprints should match')

    def test_and_start_activities(self):
        xor_tree = "+( X( τ, 'c' ), ->( X( τ, 'a' ), 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_start = [['c', 's'], ['a', 's'], ['b', 'ns']]
        self.assertListEqual(sa, expected_start, msg='start activities should match')

    def test_and_end_activities(self):
        xor_tree = "+( X( τ, 'c' ), ->( X( τ, 'a' ), 'b' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_end = [['c', 's'], ['b', 'ns']]
        self.assertListEqual(ea, expected_end, msg='end activities should match')




    def test_seq_actvities(self):
        xor_tree = "->( 'a', X( τ, 'b' ), 'c', 'd' )"
        tree = parse(xor_tree)
        footprint = fp.extract_footprint_from_process_tree(tree)
        activities = []
        activities.append("a")
        activities.append("b")
        activities.append("c")
        activities.append("d")
        for i in range(4):
            self.assertEqual(activities[i], footprint.index[i], msg="activities should match")

    def test_seq_sequential_footprints(self):
        xor_tree = "->( 'a', X( τ, 'b' ), 'c', 'd' )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_sequential = [(['a', 'ns'], ['b', 's']), (['a', 'ns'], ['c', 'ns']), (['b', 's'], ['c', 'ns']), (['c', 'ns'], ['d', 'ns'])]
        self.assertListEqual(footprints[0], expected_sequential, msg="Sequential footprints should be empty")

    def test_seq_parallel_footprints(self):
        xor_tree = "->( 'a', X( τ, 'b' ), 'c', 'd' )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_parallel = []
        self.assertListEqual(footprints[1], expected_parallel,  msg='parallel footprints should match')

    def test_seq_start_activities(self):
        xor_tree = "->( 'a', X( τ, 'b' ), 'c', 'd' )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_start = [['a', 'ns']]
        self.assertListEqual(sa, expected_start, msg='start activities should match')

    def test_seq_end_activities(self):
        xor_tree = "->( 'a', X( τ, 'b' ), 'c', 'd' )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_end = [['d', 'ns']]
        self.assertListEqual(ea, expected_end, msg='end activities should match')



    def test_loop_actvities(self):
        xor_tree = "*( ->( 'a', 'b', 'c' ), X( τ, 'd' ) )"
        tree = parse(xor_tree)
        footprint = fp.extract_footprint_from_process_tree(tree)
        activities = []
        activities.append("a")
        activities.append("b")
        activities.append("c")
        activities.append("d")
        for i in range(4):
            self.assertEqual(activities[i], footprint.index[i], msg="activities should match")

    def test_loop_sequential_footprints(self):
        xor_tree = "*( ->( 'a', 'b', 'c' ), X( τ, 'd' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_sequential = [(['a', 'ns'], ['b', 'ns']), (['b', 'ns'], ['c', 'ns']), (['c', 'ns'], ['d', 's']), (['c', 'ns'], ['a', 'ns']), (['d', 's'], ['a', 'ns'])]
        self.assertListEqual(footprints[0], expected_sequential, msg="Sequential footprints should be empty")

    def test_loop_parallel_footprints(self):
        xor_tree = "*( ->( 'a', 'b', 'c' ), X( τ, 'd' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_parallel = []
        self.assertListEqual(footprints[1], expected_parallel,  msg='parallel footprints should match')

    def test_loop_start_activities(self):
        xor_tree = "*( ->( 'a', 'b', 'c' ), X( τ, 'd' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_start = [['a', 'ns']]
        self.assertListEqual(sa, expected_start, msg='start activities should match')

    def test_loop_end_activities(self):
        xor_tree = "*( ->( 'a', 'b', 'c' ), X( τ, 'd' ) )"
        tree = parse(xor_tree)
        sa, ea, footprints, activities, skippable = fp.extract_footprints(tree)
        expected_end = [['c', 'ns']]
        self.assertListEqual(ea, expected_end, msg='end activities should match')

if __name__ == '__main__':
    unittest.main()
